# Quantum Computer GUI

*This was created as a demo for our Jugend Forscht project*

With this GUI you can submit a variety of chess problems to a real quantum computer (via dwave leap) or solve it locally on your own hardware.
You can also provide your own optimization problem for the quantum computer to solve.

# Installation
1. Install python 3
2. Install the dwave oceans toolkit
3. Install PyQt4 and numpy
4. Clone this repository
5. Run jufo-gui.py with python 3
